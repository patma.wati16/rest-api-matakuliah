package main

import (
	"fmt"
	"log"
	"net/http"
	"encoding/json"
	"io/ioutil"
	//"database/sql"
	"github.com/gorilla/mux"
	//"github.com/go-sql-driver/mysql"
)

type Matakuliah struct {
	Id		string `json:"id_mk"`
	MK   	string `json:"nm_mk"`
	SKS 	int `json:"sks"`
	SMST	int `json:"smst"`
}

var Matakuliahs []Matakuliah

func homePage(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Welcome to Home Page, Patma")
	fmt.Println("Endpoint Hit: HomePage")

}

func returnAllMatakuliahs(w http.ResponseWriter, r *http.Request){
	fmt.Println("Endpint Hit : returnAllMatakuliahs")
	json.NewEncoder(w).Encode(Matakuliahs)
}

func returnSingleMatakuliahs(w http.ResponseWriter, r *http.Request){
	vars := mux.Vars(r)
	key := vars["id_mk"]

	for _, matakuliah := range Matakuliahs {
		if matakuliah.Id == key {
			json.NewEncoder(w).Encode(matakuliah)
		}
	}
}

func createNewMatakuliahs(w http.ResponseWriter, r *http.Request){
	reqBody, _ := ioutil.ReadAll(r.Body)
	var matakuliah Matakuliah
	json.Unmarshal(reqBody, &matakuliah)
	Matakuliahs = append(Matakuliahs, matakuliah)
	json.NewEncoder(w).Encode(matakuliah)
}

func deleteMatakuliahs(w http.ResponseWriter, r *http.Request){
	vars :=mux.Vars(r)
	key := vars["id_mk"]

	for index, matakuliah := range Matakuliahs {
		if matakuliah.Id == key {
			Matakuliahs = append(Matakuliahs[:index], Matakuliahs[index+1:]... )
		}
	}
}
func handleRequests() {
	//http.HandleFunc("/", homePage)
	//log.Fatal(http.ListenAndServe(":1000", nil))
	myRouter := mux.NewRouter().StrictSlash(true)
	myRouter.HandleFunc("/",homePage)
	myRouter.HandleFunc("/matakuliahs", returnAllMatakuliahs)
	myRouter.HandleFunc("/matakuliah", createNewMatakuliahs).Methods("POST")
	myRouter.HandleFunc("/matakuliah/{id_mk}", deleteMatakuliahs).Methods("DELETE")
	myRouter.HandleFunc("/matakuliah/{id_mk}", returnSingleMatakuliahs)
	log.Fatal(http.ListenAndServe(":1000", myRouter))
}

func main() {
	//
	Matakuliahs = [] Matakuliah{
		Matakuliah{Id: "MK01", MK: "GOLANG", SKS: 2, SMST: 2},
		Matakuliah{Id: "MK02", MK: "REST API", SKS: 2, SMST: 5},
	}
	handleRequests()

}
//patmawati